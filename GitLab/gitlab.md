# GitLab
Git est un protocol (un support) utilise par plusieurs sites comme GitLab 
Outil pour gérer les version d’un logiciel si on travaille à plusieurs

## Creation repository + envoyer a Git 
Endroit on va mettre un projet complet 
Fichier readme.md 

### Creer un repo sur gitLab 
Apres creation, cliquer sur clone pour obtenir l’url

### Acceder au Terminal 
``git innit + entrer``: donner la possibilite a un repository de connecter a Git 
Si le dossier apparaît en rouge, il n’est pas pris en compte par Git. il faut faire click droit >Git > Add pour cela 

### Envoyer a Git = faire un commit
Signaler qu’on ajouter quelque chose avec un message “add new readme file” 
*Toujours coder en anglais*    
Click commit and push (les modification sont envoyees dans le cloud) 
> copier l’url du dossier gitLab 

# Comment ouvrir un projet git existant grâce a Webstrom

## Aller chercher le git
Click clone > clone with https 
Une fois une webstorm, click _VCS > checkout from version control_ > git > coller l’url 
Directory = nom du projet sur notre disque dur

## Modifier dans webstrom 
Une fois modifié, le fichier est bleu (vert pour les fichiers neufs) 
Comit encore une fois 

# Branch sur Git 

Sur Git, la Master est la version de reference, qui a ete validee. Lorsqu’on travaille a plusieurs, on cree des branch, c.a.d des copies de la master. Puis on demande validation que ces modifications soient integrees a la Master.

## Creation de Branch 
Clic “Git:master” sur webstrom > new branch
Pour editer une feuille, il faut la checkout
*ne pas checkout une feuille avant de l’avoir commit*

## Envoyer la branch vers la Master 
Sur le site gitlab, sur la page principale du projet 
Click ‘create merge request’ > faire une autorisation d’envoi qui est valide par le chef de projet 

## Récupérer les modifs sur webstrom
Click “update” (fleche bleue en haut a droite) pour pull les update
Click update type = merge

# Configurer Git afin qu’il fonctionne bien avec Webstrom 

## Configurer le Terminal 
Google: git set username and password > aller sur le site atlassian.com
> copier la commande de username et coller dans le terminal Webstrom > mettre nom et prenom propre
> paeil pour email (utiliser celle enregister sur gitlab) 
> Aller sur webstrom> preferences. Si GitLab apparait deja dans Version Control, il faut installer le plugin GitLab Projects 
> Redemarrer Webstorm 
> Retourner sur preferences > Add new GitLab Server > url gitlab.com > creer un token en cliquant sur lien (scope: api) > copier le token, coller sur Webstrom

