# Row et Col
*Toujours avoir des row avec des col, interdit de faire sans. Une col est toujours contenue dans une row, et pas l’inverse*

## Creation d’une colonne
Raccourci: Div.col+tab 

```
<div class=”row content”>
<div class=”col”></div
<div class=”col”></div>
</div>

   <div class=”row content”
          <div class=”col”></div> 
          <div class=”col”></div>
</div>
```

## Creation menu et contenu
Les classes css sont crees a l’avance 
Une fois ajoutés au HTML, la couleur d'origine est remplacée car le framework est conçu pour que les col prennent toute la place dispo.

```
<div class=”row content”>
<div class=”col my-menu”></div

<div class=”col my content”></div>
</div>
```


### Decouper la page 1 tiers, 2 tiers 
Il y a 12 colonnes possibles de largeur dans une page. 
*Donc 2tiers c 4 colonnes sur 12*  

```
<div class=”row content”>
<div class=”col-4 my-menu”></div>

<div class=”col-8 my content”></div>
</div>
```

  
### Décaler une col a gauche (si on utilise pas toutes les 12 colonnes) 
<div class=”col-4 offset-1 my-menu”></div>

Bootstrap rempli toujours les cols a gauche. Donc pour mettre une col a droite, suffit de la laisser vide

### Ajouter un style a chaque row
 
```css
.part-1 {
Background: yellow
}
```

```css
.part 2 {
Background: blue
}
```

Ajouter au html: 
```
<div class=”row part-1”>
<div class=”col”></div>
coucou1
</div>

<div class=”row part-2”>
<div class=”col”></div>
coucou2
</div>
```

*La couleur ne s’affiche pas si le row est vide. Il faut un contenu (un espace vide est possible > &nbsp ). Ce n’est pas le cas de la col, car elle a une height, et donc n’est pas vide*
