# Containers fluides et containers classiques

## Container classique 

### Creer classes css 

```css
.main {
background: black; 
}

.content {
height: 800px;
}
```

### Les appliquer au html

```html
<div class=”container main”
<div class=”row content”>
<div class=”col”>coucou</div>
<div class=”col”>2</div>
<div class=”col”>3</div>
</div>`

</div>
```

*Le container classique est toujours centré*


## Container fluid
`<div class=”container-fluid main”`

*La le container prends toute la longueur de la page.
