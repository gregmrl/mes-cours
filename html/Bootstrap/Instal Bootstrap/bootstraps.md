### BOOTSTRAP

# Installer Bootstrap

## Bootstrap intro 
Framework, cadre de travail. Il faut suivre sa façon de travailler. 

### Mon CV 
Bonne pratique, découper le site en dossiers contenant des pages. 
Main
Xp (experiences) 
Assets (dossiers non importants) 



### Terminal permet de donner des commandes sans code
Telecharger Node, ensuite faire commande *npm init* (store avec outils HTLM et CSS) 
Ne pas avoir le même nom 
Nouveau dossier package.json cree automatiquement

### npm install 
Commande d’installer bootstrap
*faire attention à l’orthographe*
Nouveau dossier node_modules avec bootstrap dedans 

### Activer Bootstrap - fichier index.html
Balise _link+tab_   
Aller chercher la feuille de style bootstrap
`href=”../” (pour remonter au dossier correct)

### Utiliser Bootstrap 
Toujours avoir une div, class container 

#### Ligne avec des colonnes à l'intérieur <div class=”row”>
```html
<div class=”col”>1</div>
<div class=”col”>2</div>
<div class=”col”>3</div>
</div>
```


#### Créer une nouvelle feuille de style css pour centrer aussi les noms 1 2 3 des colonnes

``
.number: text-align
``

#### Ensuite appliquer le style que je viens de créer 
```html
<div class=”row”>
<div class=”col numbers”>1</div>
<div class=”col numbers”>2</div>
<div class=”col numbers”>3</div>
</div>
```
