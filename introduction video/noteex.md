### INTRODUCTION CODE

- [Vidéo 1](https://www.youtube.com/watch?v=CKhsOphcXHQ&list=PLwZsu4cvOoNifDOiqznZpkVxrjp6VeGl7&index=2)
#### Installation de Web Storm
- **Télécharger l'IDE WEBSTORM**

WebStorm est un IDE pour les langages Web
(HTML, CSS et JavaScript), développé par l'entreprise JetBrains 
et basé sur la plateforme IntelliJ IDEA.

Cliquer ici sur "WEBSTROM" pour basculer sur le site de téléchargement :

[WEBSTORM](https://www.jetbrains.com/fr-fr/webstorm/download/#section=windows)



#### Raccourcis clavier 
```
shift + flèche / selectionner texte
shift + fin / selection d'un mot
shift + début / selection d'un mot
ctrl + c, v, x / copier, coller, couper
ctrl + d / dupliquer ligne
ctrl + z / revenir en arrière
shift + ctrl + z / annuler retour en arrière
ctrl + alt + s / voir les préférences dans WEBSTORM
ctrl +`y / supprimer ligne
```
