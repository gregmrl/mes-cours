# Firebase presentation

Outil compose par Google. AAAS (Architecture as a Service). Permet de creer site et apps avec le moins d’effort possible > sans avoir à le manager soi même. 

## Access 
Aller sur le site firebase > accéder à la console > créer un projet > créer compte google analytics 

## Hosting - hebergement 
Aller chercher notre site bootstrap. Aller sur firebase tolls 
> ouvrir Terminal, lancer commande “npm install -g firebase-tools”
-g veut dire qu'on installe un logiciel non pas l'intérieur d’un projet, mais en general cad dans la machine. On peut l’utiliser comme ligne de commande après installation 


## Créer une page principale d'accès au nouveau site

```html
<body>

<h1>Mon menu</h1>
<ul>
<li>
<a href =”./main/index.html”>main</a>
</li>

<li>
<a href =”./xp/index.html”>main</a>
</li>
</ul>

</div>

</body>
```

## Heberger le site sur firebase 

Aller sur le Terminal, commande “firebase login” > puis “firebase init hosting” > puis utiliser projet existant (ou créer nouveau)

> quel est votre directory public? “./” signifie la racine du site. Normalement un dossier spécial avec l’export du site doit etre creee. 
>un nouveau fichier est créé “firebase.json” + “.firebaserc” (permet de savoir dans quel fichier nous avons utilisé notre fichier)
> dans les fichiers a ignorer, ajouter les fichiers package
> commande “firebase deploy”
Tous les fichiers html et css sont envoyés sur un serveur, et le site est dispo en ligne

## Problems
Onglet “Network” de la console. On se rends compte que bootstrap n’est pas pris en compte. 
> dans le dossier assets, créer un autre dossier “vendors” (nom traditionnel pour les dossiers venant d’une tiers personne) 
> aller chercher le dossier “dist” de vendors et jquery, et renommer “bootstrap”
> dans le html, indiquer le nouveau chemin vers nouveau dossier bootstrap
>refaire le deploy
> Une fois sur firebase, les deux versions sont disponibles 
