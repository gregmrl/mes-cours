Comment noter sur Md? 

> Titre principal: #
> Sous titre: ##
> Sous-partie: ###
> Noter du code: ``
> Gros bout de code: ```
> Italique: _text_
> Bold: *text*


Examples

# Introduction
## Partie 1 - Qu’est ce que le md ? 
TEXT TEXT TEXT 

## Partie 2 - Comment utiliser md? 
### Pour faire ca
TEXT TEXT TEXT 

### Pour faire aussi ca 
TEXT TEXT TEXT
